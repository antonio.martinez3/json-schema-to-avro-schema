# JSON schema to AVRO schema

This is a simple tool to convert a JSON schema to an AVRO schema.

## Installation

```bash
npm install
```

## Usage

```bash
npm run translate -- <path-to-first-json-schema> <path-to-second-json-schema> <...>
```

## Example

```bash
npm run translate -- ./example-one.schema.json ./example-two.schema.json
```

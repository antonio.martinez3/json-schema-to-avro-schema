import fs from "fs";
import jsonSchemaAvro from "jsonschema-avro";

process.argv.slice(2).forEach((jsonSchemaPath) => {
  const avroSchemaPath = jsonSchemaPath
    .split(".schema.json")[0]
    .concat(".avsc");
  console.log("Converting", jsonSchemaPath, "to", avroSchemaPath, "...");

  const jsonSchemaString = fs.readFileSync(jsonSchemaPath, "utf8");
  const jsonSchema = JSON.parse(jsonSchemaString);
  const avroSchema = jsonSchemaAvro.convert(jsonSchema);
  const avroSchemaString = JSON.stringify(avroSchema, null, 2);
  fs.writeFileSync(avroSchemaPath, avroSchemaString, "utf8");
});

console.log("Done! 🎉");
